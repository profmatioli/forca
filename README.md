**JOGO DA FORCA USANDO LISTAS LIGADAS**

O jogo deverá atender os seguintes requisitos:

1.  Deverá ter uma base de dados em arquivo CSV com palavras a serem sorteadas pelo sistema;
    O layout do arquivo deverá conter a palavra e o assunto a qual ela se refere, como a seguir:<br/>
    palavra;descricao;<br/>
    palavra;descricao;<br/>
    palavra;descricao;<br/>
    palavra;descricao;<br/>

2.  Ao iniciar o sistema deverá ser carregado o conteúdo do arquivo para uma lista;
3.  Ao iniciar uma nova tentativa, o sistema deverá escolher, aleatoriamente, uma palavra da lista, que ainda não tenha sido usada nesta seção;
4.  A palavra secreta deverá ser controlada através de uma lista, onde:
    - Carda letra da palavra seja um nó da lista;
    - Cada nó deverá conter o caracter e um campo de status para informar se a letra foi adivinhada ou não. Se foi adivinha então será mostrada.
5.  O usuário pode fazer tentativas até:
    - adivinhar a palavra secreta;
    - errar “n” vezes, quando estará enforcado;
6.  Ao terminar o jogo, exibe uma mensagem de Parabéns, caso tenha adivinhado a palavra secreta, ou de Incapacidade Intelectual, caso não tenha adivinhado e, após uma pausa, reiniciar a sessão.
7.  A cada tentativa o sistema deverá mostrar a letra sugerida pelo jogador;
8.  O sistema deverá avisar caso a letra já foi usada e recusar a sugestão;
9.  O controle das letras já usada deverá constar de uma lista ligada;
10.  A Verão 1 será mono usuário;
11. Ao iniciar uma nova seção do jogo deverá solicitar o nome do jogador;

